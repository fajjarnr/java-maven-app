def buildJar(){
    echo "Building the application..."
    sh "mvn package"
}

def buildImage(){
    echo "building the docker image..."
    withCredentials([usernamePassword(credentialsId: 'docker-hub-repo', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
        sh "docker build -t fajjarnr/test-app:jma-2.0 ."
        sh "echo $PASS | docker login -u $USER --password-stdin"
        sh "docker push fajjarnr/test-app:jma-2.0"
    }
}

def deploy(){
    echo "Deploying the application..."
}

return this