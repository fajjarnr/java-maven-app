# Jenkins Project

repo ini dibuat sebagai catatan saya dalam mempelajari CI/CD menggunakan Jenkins
berikut adalah catatan saya :

- [Instalasi Jenkins Menggunakan Docker]
- [Mounting Docker di dalam Jenkins]
- [Membuat Jenkins Pipeline]
- [Membuat Jenkins Multibranch Pipeline]
- [Jenkins Shared Library]
- [Automasi Jenkins Pipeline Jobs Menggunakan Webhook](https://medium.com/p/63d7b62b0d26/)
- [Meningkatkan Versi Aplikasi Secara Dinamis menggunakan Jenkins Pipeline]
